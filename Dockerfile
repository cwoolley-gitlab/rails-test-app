FROM ruby:3.1.0

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock /usr/src/app/
RUN bundle install

COPY . .

ENV PORT 80
EXPOSE 80

# Run Rails server:
# - with production environment
# - bound to interface 0.0.0.0
# - at port 80
# - with logging to stdout enabled
CMD ["bundle", "exec", "rails", "server", "-e", "production", "-b", "0.0.0.0", "-p", "80", "--log-to-stdout"]
